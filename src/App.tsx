import React, { useState } from 'react'
import logo from './logo.svg'
import './App.css'

function d2(x: number) {
  return x < 10 ? '0' + x : x
}

type Worker = {
  id: number
  name: string
}

type State = {
  [staff_id: number]: {
    [isoDate: string]: string
  }
}

function App() {
  const MaxDay = 31
  const Days = new Array(MaxDay).fill('')
  const NDay = Days.length
  const workers: Worker[] = [
    { id: 1, name: 'Alice' },
    { id: 2, name: 'Bob' },
    { id: 3, name: 'Cherry' },
  ]
  const year = 2021 // full year
  const month = 8 // 0..11
  function makeState(): State {
    let state: State = {}
    workers.forEach(worker => {
      state[worker.id] = {}

      let day = 1
      for (;;) {
        let date = new Date()
        date.setFullYear(year, month, day)
        if (date.getMonth() !== month) {
          break
        }
        date.setHours(0, 0, 0, 0)
        let isoDate = date.toISOString()
        state[worker.id][isoDate] = ''

        day++
      }
    })

    return state
  }
  const [state, setState] = useState(makeState)
  console.log('state:', state)

  const NWorker = workers.length
  const [tdList, setTdList] = useState<(HTMLTableDataCellElement | null)[][]>(
    new Array(NWorker).fill(0).map(() => new Array(NDay).fill(null)),
  )
  return (
    <div className="App">
      <header className="App-header">
        <button
          onClick={() => {
            console.log(state)
          }}
        >
          submit
        </button>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              {Days.map((_, i) => (
                <th key={i}>{d2(i + 1)}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {workers.map((worker, workerIndex) => (
              <tr key={workerIndex}>
                <td>{worker.name}</td>
                {Days.map((_, dayIndex) => (
                  <td
                    ref={e => {
                      tdList[workerIndex][dayIndex] = e
                      // if (!e) return
                      // if (tdList[workerIndex][dayIndex] === e) return
                      // let newTdList = [...tdList]
                      // newTdList[workerIndex] = [...newTdList[workerIndex]]
                      // newTdList[workerIndex][dayIndex] = e
                      // setTdList(newTdList)
                    }}
                    className="input"
                    key={dayIndex}
                    contentEditable
                    onInput={event => {
                      let element = event.target as HTMLElement
                      let text = (element.textContent || '').trim().slice(0, 2)
                      if (element.textContent !== text) {
                        element.textContent = text
                      }
                      let date = new Date(year, month, dayIndex + 1)
                      date.setHours(0, 0, 0, 0)
                      let isoDate = date.toISOString()
                      state[worker.id][isoDate] = text
                      setState(state)
                    }}
                    onKeyDown={e => {
                      // console.log({
                      //   workerIndex,
                      //   dayIndex,
                      // })
                      let target: HTMLTableCellElement | null = null
                      switch (e.key) {
                        case 'ArrowUp':
                          if (workerIndex === 0) return
                          target = tdList[workerIndex - 1][dayIndex]
                          break
                        case 'ArrowDown':
                          if (workerIndex === workers.length - 1) return
                          target = tdList[workerIndex + 1][dayIndex]
                          break
                        case 'ArrowLeft':
                          if (dayIndex === 0) return
                          target = tdList[workerIndex][dayIndex - 1]
                          break
                        case 'ArrowRight':
                          if (dayIndex === MaxDay - 1) return
                          target = tdList[workerIndex][dayIndex + 1]
                          break
                        default:
                          return
                      }
                      target?.focus()
                    }}
                  ></td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </header>
    </div>
  )
}

export default App
